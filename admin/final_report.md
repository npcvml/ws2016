# Nepal Computer Vision and Machine Learning Winter School 2016

## Who should read this report

1. Future organisers of NPCVML.

1. Others who wish to start something like NPCVML for their own domain of expertise.

1. Audience interested in reading about NPCVML.

1. Students who attended the NPCVML 2016.

## NPCVML Objectives and Values

### Objectives

1. To inspire undergraduate and graduate students in Engineering Sciences to pursue the field of Computer Vision and Machine Learning

### Values

1. Ensure minimal attendance fee to ensure that finance is not an obstacle for students

1. Ensure that all the course contents (slides, codes, videos etc.) of this winter school are openly accessible to everyone without any restriction (to be released under an open source license)


## Format 

The school was run for two days that included:

1. Three key lecture sessions:
   1. Mathematics in Computer Vision and Machine Learning
   1. Introduction to Computer Vision
   1. Introduction to Machine Learning

1. Two paper reading sessions:
   1. Khanal B. and Sidibe D. (2011), ICIRA 2011. Efficient Skin Detection under Severe Illumination Changes and Shadows. [pdf](https://gitlab.com/npcvml/ws2016/blob/master/slides/paper_reading/khanal2011skindetection/khanal2011skindetection.pdf)
   1. Turk M. and Pentland A. (1999). Eigenfaces for recognition. [pdf](http://www.face-rec.org/algorithms/PCA/jcn.pdf)
   
1. Sharing of project ideas, career prospects and discussion session.

## Organizational Aspects (@Bishesh)

1. 40 students registered online but only 21 attended the two day winter school.
   * 20 undergraduate (runnig + completed) + 1 postgraduate students.
   * 5 female + 16 male.
   * 3 Kathmandu University + 15 Pulchowk Campus + 1 Patan Campus + 1 Kathmandu Engg. College + 1 Kathford International College.
	
1. Pulchowk campus was a good choice for venue
   1. Cost: pay for cleaning of the classroom, tea for all the attendees, etc.
   1. Internet connection, projector, chair and desks, whiteboard, etc were in excellent condition and helped run the winter school very smoothly.
   
1. Making all the course material publicly available once the course was completed. [Available here](https://gitlab.com/npcvml/ws2016).
   
1. Dissemination and advertisement
   1. Used facebook and twitter. [website](https://www.npcvml.org), [twitter](https://www.twitter.com/npcvml), and [facebook](https://www.facebook.com/npcvml/)

## What worked (@Bishesh)

1. Students were very interested in learning Linear Algebra.
   1. Students found our focus on the very basics of Linear Algebra very insightful where we presented the material in a different way than what is usually done in most Nepali universities.
The presentation closely followed Gilbert Strang's linear algebra book.
   1. Very positive feedback on live demonstration of PCA with a python notebook

1. Paper reading session was a hit because students were quite engaged and interested during the full 2+2 hour session
   1. We demonstrated with example the fundamentals about how to read and write scientific papers. 
   1. Live code explanation and demo made it more interesting.
   1. Distribution of printed copies of the discussed papers was very helpful for students to annotate while reading and discuss ideas being discussed.
 
 
## What did not work (@Abhishek)

1. Sharing project ideas was a little bit engaging but did not hit the right tone with all the students, some felt lost and had to be pushed to share their ideas and participate in the discussion.

1. Discussing career prospects session was not well organized and did not result in much useful discussion and advice for the attendees.

1. Took more time than anticipated to finalize the contents of winter school and prepare the slides.

1. The academic level of students varied from 1st year undergraduate to a ME graduates. 

1. The presentations were quite interesting for students.
In hindsight, we could have prepared short coding exercise to be solved and discussed during the presentation.
This would have further engaged the students in the topic (Computer Vision, Machine Learning, Linear Algebra).

1. Seems that the majority of students were from Pulchowk Campus.
We wanted to have more diverse participation from other colleges and KU. 

## Recommendations for future organizers

1. Increasing the number of sessions; 4-5 days workshop.

1. Preparing the course material takes time, start early!!

1. Try to focus on: inspiring, showing what is possible, giving solid foundations, and guiding where to explore more.

1. Try to make sure that there is wider participation and not just from one particular institute.


   


