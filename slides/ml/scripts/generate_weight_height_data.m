% Generate synthetic height/weight dataset
%
% Author: Abhishek Dutta <http://abhishekdutta.org>
% Nov. 11, 2016


close all; clear; clc;

pkg load statistics;

N = 500;

% male : weight, height
mu = [60 162.9];
sigma = [40 6.2; 4.1 7.42];
dm = mvnrnd(mu, sigma, N);

% female : weight, height
mu = [50 154.9];
sigma = [25 6.8; 0.0 7.11];
df = mvnrnd(mu, sigma, N);

figure(1);
plot( dm(:,1), dm(:,2), '.r'); hold on;
plot( df(:,1), df(:,2), '.b'); hold off;


