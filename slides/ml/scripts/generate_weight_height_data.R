library('ggplot2')

dm <- read.csv('../data/male.csv', header=FALSE)
df <- read.csv('../data/female.csv', header=FALSE)

d <- data.frame(weight=dm[,1], height=dm[,2], gender='male')
d <- rbind(d, data.frame(weight=df[,1], height=df[,2], gender='female'))

write.csv(d, file='../data/weight_height_data.csv', quote=FALSE, row.names=FALSE)

p0 <- ggplot(d, aes(x=weight, y=height, color=gender))
p0 <- p0 + geom_point() + theme_bw()
p0 <- p0 + scale_x_continuous( 'Weight (kg)' )
p0 <- p0 + scale_y_continuous( 'Height (cm)' )
p0 <- p0 + scale_color_discrete( name='')

p0 <- p0 + theme(axis.title.x = element_text(size = 12, vjust = -0.25)) + theme(axis.title.y = element_text(size = 12, angle = 90, vjust=1.6)) + theme(axis.text.x = element_text(size = 9, angle = 0)) + theme(axis.text.y = element_text(size = 9)) + theme(legend.key = element_blank(), legend.text = element_text(size = 10))
p0 <- p0 + theme(legend.position="top", legend.direction = "horizontal")
#p0 <- p0 + guide_legend(title.position='top', label.position='top', nrow=1, direction='horizontal', byrow=TRUE)
#p0 <- p0 + theme(legend.position=c(0.94,0.2))

plot_filename <- '../images/weight_height_dataset'
pdf(paste(plot_filename, 'pdf', sep='.'), width = 6, height = 4, useDingbats = FALSE, title = tail(strsplit(plot_filename, '/')[[1]],1))
print(p0)
dev.off()
embedFonts(paste(plot_filename, 'pdf', sep='.'), options="-dPDFSETTINGS=/printer")

cat('\nFinished\n')
