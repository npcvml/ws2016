\documentclass[10pt]{beamer}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%

%\vec command will create boldface instead of arrows
\let\oldhat\hat
\renewcommand{\vec}[1]{\mathbf{#1}}
\renewcommand{\hat}[1]{\oldhat{\mathbf{#1}}}

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}
\usepackage[absolute,overlay]{textpos} % absolute positioning of texts and images
\setlength{\TPHorizModule}{1mm}
\setlength{\TPVertModule}{\TPHorizModule}

\usepackage{hyperref}
\usepackage{listings}
\hypersetup{
    pdftoolbar=false,        % show Acrobat’s toolbar?
    pdfmenubar=false,        % show Acrobat’s menu?
    pdffitwindow=true,     % window fit to page when opened
    pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={},    % title
    pdfauthor={Abhishek Dutta and Bishesh Khanal},     % author
    pdfsubject={NPCVML Winter School 2016},   % subject of the document
    pdfcreator={Abhishek Dutta and Bishesh Khanal},   % creator of the document
    pdfproducer={pdfLateX}, % producer of the document
    colorlinks=false,       % false: boxed links; true: colored links
}
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}


\title{Introduction to Machine Learning}
\subtitle{}
\author{Dr. Abhishek Dutta and Dr. Bishesh Khanal}
\date{\small{Nov. 14 and 15, 2016}}

\beamertemplatenavigationsymbolsempty
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Contents}
\tableofcontents
\end{frame}
\section{Introduction}
\AtBeginSection[]
{
\begin{frame}{Table of Contents}
\tableofcontents[currentsection]
\end{frame}
}
\begin{frame}[fragile]
\frametitle{Machine Learning}
\begin{center}
\Large{Algorithms that can improve their performance using training data}
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{Most common tasks in Machine Learning?}
\begin{block}{\visible<2->{Supervised Learning}}
\begin{itemize}
\item Classification
\item Regression
\end{itemize}
\end{block}

\begin{block}{\visible<3->{Unsupervised Learning}}
\begin{itemize}
\item Clustering
\item Dimensionality reduction (E.g. PCA)
\end{itemize}
\end{block}
\visible<4->{
\textbf{Supervised learning: } Available training data, i.e. labelled inputs and outputs\\
\textbf{Unsupervised learning: } No training data, i.e. no labelled inputs and outputs
}
\end{frame}

\begin{frame}[fragile]
\frametitle{Machine Learning Map}
\includegraphics[width=\linewidth]{./images/ml_map_scikit}\\
\tiny{Source: \url{http://scikit-learn.org/stable/tutorial/machine_learning_map/}}
\end{frame}

\section{A walk through key ideas}
\begin{frame}[fragile]

\frametitle{Reference}
\begin{itemize}
\item An Introduction to Statistical Learning with applications in R (ISLR) by Gareth James et al. 
\end{itemize}
\end{frame}

\begin{frame}{fragile}
\frametitle{Data}
\alert{Machine learning does not exist without data!}
\pause
\begin{block}{Advertising dataset}
  \begin{itemize}
  \item \texttt{sales} of a product in 200 different markets 
  \item Each market has advertising budgets for the product 
  \item Advertising budgets in three media: \texttt{TV}, \texttt{radio} and \texttt{newspaper} 
  \end{itemize}
\end{block}
\visible<2->{
\includegraphics[width=\linewidth]{./images/advertising_dataset}\\
\tiny{Unit in 1000s; blue line: Least squares fit. Source: ISLR James et al.}
}
\end{frame}


\begin{frame}[fragile]
\frametitle{Data, Variables, and Prediction }
\begin{block}{Looking for Answers in Data}
\begin{itemize}
\item Which mass media to focus on ?
\item Does more expenditure on TV increase \texttt{sales} ?
\item \alert{Predict} \texttt{sales} from the basis of three media budgets
\end{itemize}
\end{block}

\pause
\begin{block}{Predict \texttt{sales} from advertising budgets}
  \begin{itemize}
  \item Advertising budgets: \textit{input variables}, usually denoted by $X$
  \item $X$ also known as: \textit{predictors, independent variables, features}
  \item Example: \texttt{TV}: $X_1$, \texttt{radio}: $X_2$, \texttt{newspaper}: $X_3$\pause
  \item \texttt{sales}: \textit{output variables}, usually denoted by $Y$
  \item $Y$ also known as: \textit{response variable, dependent variable}
  \end{itemize}
\end{block}
\end{frame}

\begin{frame}[fragile]
\frametitle{Modelling the Input Output Relation}
E.g. we observe a response $Y$, and $p$ different predictors: $X_1$, $X_1$, ...,$X_p$\\
A relationship between $Y$ and $X = (X_1, X_1, ...,X_p)$
\[
Y = f(X) + \epsilon
\]
\begin{itemize}
\item $f$ some fixed but unknown function of $X_1$, $X_2$, ...,$X_p$
\item $\epsilon$ a random \textit{error term}, independent of $X$ and mean zero
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Estimating $f$}
\begin{textblock}{50}(78.5, 28)
\includegraphics[width=50mm]{./images/income_fx}\\
\end{textblock}

$Y = f(X) + \epsilon$\\
\begin{itemize}
\item Blue surface: true underlying $f(X)$ (simulated data!)
\item Red dots: observed values of the income for 30 individuals
\end{itemize}
\pause
\begin{block}{Prediction}
  \begin{itemize}
  \item \textbf{Estimate} $f$ to get $\hat f$
  \item For any $X$, \textbf{predict} $\hat Y = \hat f(X)$
  \item $\hat f$ acts as black box once estimated
  \end{itemize}
\end{block}
\pause
\begin{block}{Reducible and irreducible error}
  \begin{itemize}
  \item Get $\hat Y$ closer to $Y$ by making $\hat f$ close to $f$
  \item \textbf{Reducible error:} Arising from inaccurate estimate $\hat f$\pause 
  \item Even if $\hat f = f$, $\hat Y = Y$ ($\epsilon$ independent of $X$)
  \item \textbf{Irreducible error:} variability associated to $\epsilon$
  \item E.g. $\epsilon$ could contain unmeasured variable useful in predicting $Y$
  \end{itemize}
\end{block}
\end{frame}

\begin{frame}[fragile]
\frametitle{Estimating $f$}
$Y = f(X) + \epsilon$\\
\begin{block}{Inference}

Understanding the way $Y$ is affected when $X$ changes
  \begin{itemize}
  \item Which predictors are associated with the response ?
  \item What is the relationship between the response and each predictor ?
  \item How complex is the relationship ?
  \end{itemize}
\end{block}
\end{frame}


\begin{frame}[fragile]
\frametitle{How do we estimate $f$ ?}
\hspace{-30mm}{\[Y = f(X) + \epsilon\]}
\begin{textblock}{50}(78, 8)
\includegraphics[width=50mm]{./images/income_fx}\\
{\hspace{20mm}\tiny{Source: ISLR James et al.}}
\end{textblock}
\vspace{10mm}
\begin{block}{Training data}
  \begin{itemize}
  \item Supervised Learning
  \item Observed $n$ different data points having $p$ different predictors
  \item E.g. $n = 30$ points with $p = 2$ predictors
  \item Training data: $\{(x_1, y_1), (x_2, y_2), ..., (x_n, y_n)\}$ where,
  \item $x_i = (x_{i1}, x_{i2}, ..., x_{ip})^T$ with $i = 1, 2, ..., n$ and $j = 1, 2, ..., p$
  \end{itemize}
\end{block}

\end{frame}

\begin{frame}[fragile]
\frametitle{How do we estimate $f$ ?}
\[Y = f(X) + \epsilon\]

\begin{block}{Parametric Models}
  \begin{itemize}
  \item \textbf{Step 1: }Make assumptions about $f$ E.g. A linear model 
\[f(X) = \beta_0 + \beta_1X_1 + \beta_2X_2 + ... + \beta_pX_p\] \pause
\vspace{-5mm}
  \item No need to estimate $p$-dimensional $f(X)$; estimate $p+1$ coefficients $\beta_0, \beta_1, ..., \beta_p$ \pause
  \item \textbf{Step 2: }\textit{Fit} or \textit{train} the model. E.g. Least squares
  \item Find $\beta$ such that \[Y \approx \beta_0 + \beta_1X_1 + \beta_2X_2 + ... + \beta_pX_p\]
  \end{itemize}
\end{block}
\end{frame}


\begin{frame}[fragile]
\frametitle{Linear Regression with Least Squares }
\vspace{-25mm}
E.g. \texttt{income} $\approx$ $\beta_0 + \beta_1 \mathtt{ X\, education} + \beta_2 \mathtt{ X\, seniority}$
\begin{block}{Parametric Models}
  \begin{itemize}
  \item \alert{:)} Reduces estimating $f$ to estimating a set of parameters
  \item \alert{:(} Assumed model may not match the real $f$
  \end{itemize}
\end{block}
\begin{textblock}{50}(10, 50)
\includegraphics[width=50mm]{./images/income_fx}\\
\end{textblock}
\begin{textblock}{50}(70, 50)
\includegraphics[width=50mm]{./images/income_lsf}\\
\tiny{Source: ISLR James et al.}
\end{textblock}

\end{frame}


\begin{frame}{Non-parametric Models}
\vspace{-20mm}
\[Y = f(X) + \epsilon\]
\vspace{-5mm}
  \begin{block}{No explicit assumptions about model}
    \begin{itemize}
    \item Make $Y - \hat f(X)$ as small as possible \textbf{without} being too rough/wigly
    \item Generally requires large number of observations
    \item \visible<2->{E.g. thin plate spine}
    \item \visible<4->{\alert{Complex models can lead to \textbf{overfitting} to noise}}
    \end{itemize}
  \end{block}

\begin{textblock}{42}(0, 55)
\visible<2->{
\includegraphics[width=42mm]{./images/income_fx}
}
\end{textblock}

\begin{textblock}{42}(42, 55)
\visible<2->{
\includegraphics[width=42mm]{./images/thin_plate_spline_smooth}\\
\tiny{Source: ISLR James et al.}
}
\end{textblock}

\begin{textblock}{42}(85, 55)
\visible<3->{
\includegraphics[width=42mm]{./images/thin_plate_spline_rough}
}
\end{textblock}
\end{frame}

\begin{frame}{Trade-off: Prediction Accuracy vs. Model Interpretability}
\begin{figure}
 \includegraphics[width=\linewidth]{./images/flexible_vs_interpretable}
\end{figure}
{\tiny{Source: ISLR James et al.}\\}

Which one to choose ? In prediction problems; in inference problems
\end{frame}

\subsection{Assesing Model Accuracy}
\AtBeginSection[]
{
\begin{frame}{Table of Contents}
\tableofcontents[currentsection]
\end{frame}
}

\begin{frame}{No free lunch in statistics}
  \begin{itemize}
  \item No \textbf{best} method that works for all problems
  \item \textbf{Challenging} Need to select a best method for a particular problem
  \item \alert{Need} to measure the quality of fit
  \end{itemize}
\pause
  \begin{block}{Measuring the quality of fit}
    \begin{itemize}
    \item E.g. Mean squared error: $MSE = \frac{1}{n}\sum_{i=1}^{n}{(y_i - \hat f(x_i))^2}$ \pause
    \item $y_i$'s are part of training set. So we have \textit{training MSE} \pause
    \item \alert{But is this interesting ?}\pause
    \item Measure error on \textit{previously unseen data} : \alert{test error}
    \item We want minimum \textit{test error} NOT training error \pause
    \item How to minimise test error ?
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Training vs. Test Error}
\includegraphics[width=\linewidth]{./images/training_vs_test_error1}\\
{\tiny{Source: ISLR James et al.}}\\
\begin{itemize}
\item \textbf{Left}: real (simulated) $f$ in black; linear and spline regressions
\item \textbf{Right}: training error in grey; test error in red 
\end{itemize}
\end{frame}

\begin{frame}{The Bias-Variance Trade-off}
Expected test MSE: sum of three fundamental quantities
\[
E\left(y_0 - \hat f(x_0)\right)^2 = \mathrm{Var}(\hat f(x_0)) + [\mathrm{Bias}(\hat f(x_0)]^2 + \mathrm{Var}(\epsilon)
\]\pause
\begin{itemize}
\item Average test MSE if $f$ estimated repeatedly using large training sets, and tested each at $x_0$
\item \alert{Good method ?} \pause \textit{low bias} AND \textit{low variance} \pause
\item \textbf{Variance:} change in $\hat f$ if estimated using a different training set
\item \textbf{Bias:} error when approximating real life problem
\end{itemize}
\end{frame}

\begin{frame}{Training vs. Test Error}
\includegraphics[width=\linewidth]{./images/training_vs_test_error1}\\
{\tiny{Source: ISLR James et al.}}\\
\begin{itemize}
\item \textbf{Left}: real (simulated) $f$ in black; linear and spline regressions
\item \textbf{Right}: training error in grey; test error in red 
\end{itemize}
\end{frame}

\begin{frame}{Training vs. Test Error}
\includegraphics[width=\linewidth]{./images/training_vs_test_error2}\\
{\tiny{Source: ISLR James et al.}}\\
\begin{itemize}
\item \textbf{Left}: real (simulated) $f$ in black; linear and spline regressions
\item \textbf{Right}: training error in grey; test error in red 
\end{itemize}
\end{frame}

\begin{frame}{Training vs. Test Error}
\includegraphics[width=\linewidth]{./images/training_vs_test_error3}\\
{\tiny{Source: ISLR James et al.}}\\
\begin{itemize}
\item \textbf{Left}: real (simulated) $f$ in black; linear and spline regressions
\item \textbf{Right}: training error in grey; test error in red 
\end{itemize}
\end{frame}

\subsection{Bias-variance Trade Off}
\AtBeginSection[]
{
\begin{frame}{Table of Contents}
\tableofcontents[currentsection]
\end{frame}
}

\begin{frame}{Bias-variance }
\includegraphics[width=\linewidth]{./images/bias_variance1}\\
{\tiny{Source: ISLR James et al.}}\\
\begin{itemize}
\item Horizontal dotted lines: $\mathrm{Var(\epsilon)}$
\item Vertical dotted lines: flexibility level corresponding to the smallest MSE.
\end{itemize}
\end{frame}

\begin{frame}{Bias-variance }
\includegraphics[width=80mm]{./images/bias_variance2}\\
{\tiny{Source: \url{http://scott.fortmann-roe.com/docs/BiasVariance.html}}}\\
\end{frame}

\subsection{Resampling Methods}
\AtBeginSection[]
{
\begin{frame}{Table of Contents}
\tableofcontents[currentsection]
\end{frame}
}

\begin{frame}{Resampling Methods}
Repeatedly draw samples from a training set and refit a model of interest\\
\vspace{2mm}
\pause
One E.g. (widely used) Cross-validation
\pause
\begin{block}{Cross-validation}
  \begin{itemize}
  \item Test data not always available
  \item Reuse training data as a test data \pause
  \item \textit{Hold out} a \textit{subset} of the training observations from fitting process
  \item Apply the fitted model to the held out subset
  \end{itemize}
\end{block}
\pause
\begin{block}{Three Examples}
  \begin{itemize}
  \item Validation set hold-out set
  \item Leave-one-out cross-validation
  \item k-fold cross-validation
  \end{itemize}
\end{block}
\end{frame}

\begin{frame}
  \frametitle{Validation Set Hold-out Set}
  \includegraphics[width=\linewidth]{./images/vsa}\\
{\tiny{Source: ISLR James et al.}}\\
$n$ observations randomly split into two halves (groups)
\begin{itemize}
\item Blue: training set
\item Orange: validation set \pause
\item Fit a model with training set
\item Predict responses for the observations in validation set
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Validation Set Hold-out Set Example}
  \includegraphics[width=\linewidth]{./images/vsa_example}\\
{\tiny{Source: ISLR James et al.}}
\begin{itemize}
\item \textbf{Left:} validation error estimates for a single split
\item \textbf{Right:} validation method repeated 10 times
\item What do we see ? \pause
\item \alert{High variablility in estimated test $MSE$}
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Leave-One-Out Cross-Validation (LOOCV)}
  \includegraphics[width=\linewidth]{./images/loocv}\\
{\tiny{Source: ISLR James et al.}}\\
$n$ observations split into training set with $n-1$ observations + test set with $1$ observation
\begin{itemize}
\item Take mean $MSE$ of $n$ $MSE$ estimates.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{k-Fold Cross Validation (k-fold CV)}
  \includegraphics[width=\linewidth]{./images/kfcv}\\
{E.g. with $k=5$. \tiny{Source: ISLR James et al.}}\\
\begin{block}{$n$ observations split into $k$ groups}
\begin{itemize}
\item First fold validation set
\item $k-1$ folds training set
\item Take sum of $k$ $MSE$ estimates
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
  \frametitle{LOOCV vs k-fold CV}
  \includegraphics[width=\linewidth]{./images/loocv_vs_kfcv}\\
{\tiny{Source: ISLR James et al.}}\\
\end{frame}

\section{A Supervised Learning Example}
\AtBeginSection[]
{
\begin{frame}{Table of Contents}
\tableofcontents[currentsection]
\end{frame}
}

\begin{frame}{Supervised Learning}
\begin{center}
\large{involves learning the relationship between example inputs and output (i.e. training data)}
\end{center}
\pause
\vfill
\begin{itemize}[<+->]
\item Classification : which category does a new observation belongs to?
\item Regression : estimate continuous response variables
\end{itemize}
\end{frame}


\begin{frame}[fragile]
\frametitle{Supervised Learning: Classification Example}
Given (weight,height) of an individual, predict the gender.
\end{frame}

\begin{frame}[fragile]
\frametitle{Gender/Weight/Height Dataset}
\begin{itemize}
\item contains weight and height of $500$ male and female
\item synthetic dataset
\end{itemize}


\pause
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{d} \hlkwb{<-} \hlkwd{read.csv}\hlstd{(}\hlstr{'data/weight_height_data.csv'}\hlstd{,} \hlkwc{header}\hlstd{=}\hlnum{TRUE}\hlstd{)}
\end{alltt}
\end{kframe}
\end{knitrout}
\pause
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{head}\hlstd{(d)}
\end{alltt}
\begin{verbatim}
##   weight height gender
## 1  64.31 156.69   male
## 2  47.18 158.88   male
## 3  47.21 162.66   male
## 4  59.14 158.42   male
## 5  65.14 161.60   male
## 6  54.65 167.15   male
\end{verbatim}
\end{kframe}
\end{knitrout}
\pause
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{summary}\hlstd{(d)}
\end{alltt}
\begin{verbatim}
##      weight          height         gender   
##  Min.   :29.62   Min.   :148.1   female:500  
##  1st Qu.:49.44   1st Qu.:154.7   male  :500  
##  Median :54.41   Median :158.9               
##  Mean   :54.71   Mean   :159.0               
##  3rd Qu.:59.87   3rd Qu.:163.2               
##  Max.   :79.39   Max.   :173.9
\end{verbatim}
\end{kframe}
\end{knitrout}
\end{frame}

\begin{frame}[fragile]
\frametitle{Gender/Weight/Height Dataset}
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\maxwidth]{figure/unnamed-chunk-2-1} 

}



\end{knitrout}
\end{frame}

\begin{frame}[fragile]
\frametitle{Logistic Regression}
\begin{equation}
h_{\theta} = g(\theta_{0} + \theta_{1} \times \textnormal{weight} + \theta_{2} \times \textnormal{height} )
\end{equation}
where, $g(.)$ is a sigmoid function
\end{frame}

\begin{frame}[fragile]
\frametitle{Logistic Regression}
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlstd{model} \hlkwb{<-} \hlkwd{glm}\hlstd{(gender} \hlopt{~} \hlstd{weight} \hlopt{+} \hlstd{height,} \hlkwc{data}\hlstd{=d,} \hlkwc{family}\hlstd{=}\hlstr{'binomial'}\hlstd{)}
\hlkwd{print}\hlstd{(model)}
\end{alltt}
\begin{verbatim}
## 
## Call:  glm(formula = gender ~ weight + height, family = "binomial", 
##     data = d)
## 
## Coefficients:
## (Intercept)       weight       height  
##  -190.20385      0.06735      1.17349  
## 
## Degrees of Freedom: 999 Total (i.e. Null);  997 Residual
## Null Deviance:	    1386 
## Residual Deviance: 259.4 	AIC: 265.4
\end{verbatim}
\end{kframe}
\end{knitrout}


\end{frame}

\begin{frame}[fragile]
\frametitle{Logistic Regression}
\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}

{\centering \includegraphics[width=\maxwidth]{figure/unnamed-chunk-4-1} 

}



\end{knitrout}
\end{frame}

\begin{frame}[fragile]
\frametitle{Testing the trained classifier}
\begin{block}{}
weight = 70 \\
height = 158 \\
gender = ?
\end{block}

\begin{knitrout}\footnotesize
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{print}\hlstd{(model}\hlopt{$}\hlstd{coefficients)}
\end{alltt}
\begin{verbatim}
##   (Intercept)        weight        height 
## -190.20384826    0.06735251    1.17349032
\end{verbatim}
\end{kframe}
\end{knitrout}

\begin{eqnarray}
h_{\theta} &=& g(-190.20384826 + 0.06735251 \times 70 + 1.17349032 \times 159) \nonumber \\
 &=& g(1.0958) \nonumber \\
 &=& 0.74947 \nonumber 
\end{eqnarray}

Since, $h_{\theta} > 0.5$, this test sample is classified as male.
\end{frame}

\section{Unsupervised Learning}
\AtBeginSection[]
{
\begin{frame}{Table of Contents}
\tableofcontents[currentsection]
\end{frame}
}

\begin{frame}{Unsupervised Learning}
\begin{center}
\large{involves finding structure in the input data}
\end{center}
\end{frame}

\begin{frame}{Unsupervised Learning: clustering}
    Easy vs. difficult
\begin{figure}
 \includegraphics[width=\linewidth]{./images/clustering_easy_difficult}
\end{figure}
\vfill
\tiny{Source: ISLR James et al.}
\end{frame}

\begin{frame}{Unsupervised Learning: K-means clustering}
\begin{figure}
 \includegraphics[width=0.5\linewidth]{./images/kmeans1.png}
\end{figure}
\textbf{Step 1:} k initial "means" (in this case k=3) are randomly generated within the data domain (shown in color).

\vfill
\tiny{source: \url{https://en.wikipedia.org/wiki/K-means_clustering}}
\end{frame}

\begin{frame}{Unsupervised Learning: K-means clustering}
\begin{figure}
 \includegraphics[width=0.5\linewidth]{./images/kmeans2.png}
\end{figure}
\textbf{Step 2:} k clusters are created by associating every observation with the nearest mean.

\vfill
\tiny{source: \url{https://en.wikipedia.org/wiki/K-means_clustering}}
\end{frame}

\begin{frame}{Unsupervised Learning: K-means clustering}
\begin{figure}
 \includegraphics[width=0.5\linewidth]{./images/kmeans3.png}
\end{figure}
\textbf{Step 3:} The centroid of each of the k clusters becomes the new mean.

\vfill
\tiny{source: \url{https://en.wikipedia.org/wiki/K-means_clustering}}
\end{frame}

\begin{frame}{Unsupervised Learning: K-means clustering}
\begin{figure}
 \includegraphics[width=0.5\linewidth]{./images/kmeans4.png}
\end{figure}
\textbf{Step 4:} Steps 2 and 3 are repeated until convergence has been reached.

\vfill
\tiny{source: \url{https://en.wikipedia.org/wiki/K-means_clustering}}
\end{frame}

\section{Reinforcement learning}
\AtBeginSection[]
{
\begin{frame}{Table of Contents}
\tableofcontents[currentsection]
\end{frame}
}
\begin{frame}[fragile]
\frametitle{Reinforcement learning}
\begin{center}
\large{learning is done via interaction with a dynamic environment}
\end{center}
\vfill
\pause
\begin{itemize}[<+->]
\item Playing a computer game (\href{https://deepmind.com/research/alphago/}{alphago}, arcade games)
\item Learning to drive an autonomous vehicle (\href{https://www.google.com/selfdrivingcar/}{google self driving car})
\end{itemize}
\end{frame}

\begin{frame}{Reinforcement learning : Examples}
\begin{center}
\Large{Learning to play the game of Pong}
\end{center}
\vfill
\begin{center}
\large{\url{http://karpathy.github.io/2016/05/31/rl/}}
\end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{Machine Learning Map}
\includegraphics[width=\linewidth]{./images/ml_map_scikit}\\
\tiny{Source: \url{http://scikit-learn.org/stable/tutorial/machine_learning_map/}}
\end{frame}

\end{document}
