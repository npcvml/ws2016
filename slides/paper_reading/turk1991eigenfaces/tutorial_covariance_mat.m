% Tutorial on covariance matrix
%
% Author: Abhishek Dutta <http://abhishekdutta.org>
% Nov. 01, 2016
%
% To install the statistics package (required for this demo)
% > pkg install io -forge
% > pkg install statistics -forge
% > pkg load statistics

close all; clear; clc;

pkg load statistics;

N = 1000;
mu = [0 0];
sigma = [0.26 0.2; 0.1 0.16];
d = mvnrnd(mu, sigma, N);

[U, S, V] = svd(d);

figure(1);
plot( d(:,1), d(:,2), '.k', 'markersize', 4); hold on;
xlim([-2 2]);
saveas(1, 'pointcloud.jpg');

figure(2);
plot( d(:,1), d(:,2), '.k', 'markersize', 4); hold on;
xlim([-2 2]);

% show principal component 1
quiver([0], [0], V(1,1), V(2,1), 'linewidth', 4);
text(V(1,1), V(2,1), strcat('Eigenvalue = ', num2str(S(1,1))), 'fontsize', 14, 'color', 'b');

% show principal component 2
quiver([0], [0], V(1,2), V(2,2), 'linewidth', 4);
text(V(1,2), V(2,2), strcat('Eigenvalue = ', num2str(S(2,2))), 'fontsize', 14, 'color', 'b');

hold off;
saveas(2, 'pointcloud_with_pc.jpg');
