# Robust skin detection 

This is an implementation of the skin detection  method presented in the 
following paper:
Khanal, B., & Sidibé, D. (2011). Efficient Skin Detection under Severe Illumination Changes and Shadows. In S. Jeschke, H. Liu, & D. Schilberg (Eds.), Intelligent Robotics and Applications: 4th International Conference, ICIRA 2011, Aachen, Germany, December 6-8, 2011, Proceedings, Part II (pp. 609–618). Berlin, Heidelberg: Springer Berlin Heidelberg.
[doi](http://doi.org/10.1007/978-3-642-25489-5_59), [pdf](https://hal-univ-bourgogne.archives-ouvertes.fr/hal-00627903/document/)

This code was used during the Paper Reading Session of the [NPCVML](https://npcvml.org) Winter School 2016.

# Reading a research paper:

## What problem is the paper focused on ?
1. Read paper's abstract.
2. Good abstract provides all the essential information without using deep or involved technical description.
3. What do you get from the first line of this paper ?

If you do not have sufficient background knowledge, it is possible that you will
get lost as you start reading the second sentence or further. Don't despair -- 
you are not expected to understand every line of the paper. The aim should be 
to first understand what problem the paper is addressing, before knowing how it does or what it proposes.

## Is this problem interesting to you or your project ? Or is the concept involved useful to you ?
1. Read the keywords, they are important.
If your interest is in skin detection, read it.
Or if log-chromaticity color space or invariance to illumination or face detection interest you, read it.
Otherwise, not necessary to read every paper that comes to you.

## Have the authors actually solved the problem ?
1. Abstract gives some hint on this.
2. In most cases, conclusion should give you a fair bit of idea on this.

*Now read the abstract and the conclusion of the paper to see whether you get the answers
of the questions we have asked so far.*

## Diagonal reading
1. Reading the section headlines to get a very very quick idea of how the paper is structures (few seconds)
2. See how the section titles are written; important to have meaningful titles for diagonal reading.


## Reading the introduction 
1. Global picture of the problem paper is addressing.
2. Existing state-of-the-art solutions to the problem and limitations they have.
3. Clearly define the *scientific gap* that exists in the problem and targeted solution.
4. Outline the approach of the paper; how it intends to close or reduce this *gap*.

*Now read the introduction to see these four elements*

## Technical details of the solution or the proposed method
1. Quite normal to not understand many things on the first read:
even the highly experienced experts do not necessarily understand the paper in one reading.
2. Skim through the paper once or twice and take note of what you understood.
3. Is this enough to quickly understand what the paper does or how the method/solution works ?
4. Can you implement the method without going on why it works ?
5. You might have to read few other cited papers to fully understand everything.
Whether you have to or not depends on your need, goals and level of understanding.

## Limitations of the proposed method/solution ?
1. Look for it in the discussion and/or conclusion section.
2. Conference vs. journal articles: conference articles means very limited space but
journal articles usually have much more space to detail the limitations.

*Can you make a critical remark on the paper regarding the limitations ?*


## Implementation in python
   * See jupyter notebook