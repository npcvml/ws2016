# Nepal Computer Vision and Machine Learning Winter School 2016

`Note: This is a work in progress. It will be published when complete.`

## Course contents

* since we were targeting undergraduate students, we had to ensure that the winter school contents easy to understand and does not require too much of background knowledge.

* we wanted to encourage students to get into the habit of reading research papers. So we included two paper reading sessions in the winter school. We aimed to walk through two research papers and show the students how to approach this task in a systematic way.

* we know that making stuffs and working on projects are the best way to learn. So we included a "Discussing Project Ideas" session to encourage students to think of creative ways to apply the knowledge gained in CV and ML.

* @todo
  
## Paper Reading Session

We included the following two papers in the Paper Reading Session:
 1. Turk, M., & Pentland, A. (1991). Eigenfaces for recognition. Journal of cognitive neuroscience, 3(1), 71-86 [doi](https://scholar.google.com/scholar?hl=en&q=eigenfaces+for+recognition&btnG=&as_sdt=1%2C5&as_sdtp=&oq=eigenfaces+for+re), [pdf](http://www.academia.edu/download/30894770/jcn.pdf)
 2. Khanal, B., & Sidibé, D. (2011). Efficient Skin Detection under Severe Illumination Changes and Shadows. ICIRA Proceedings (pp. 609–618) [doi](http://doi.org/10.1007/978-3-642-25489-5_59), [pdf](http://bishesh.github.io/papers/Khanal_2011.pdf)

These two papers were very easy to understand and required very little background knowledge.

@todo

Abhishek Dutta and Bishesh Khanal

Jan. 2017
